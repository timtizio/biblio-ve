const express = require('express');
const basicRouter = express.Router();
const bookController = require('../src/controllers/book.controller');
const sagaController = require('../src/controllers/saga.controller');

/*

+------------------+--------+--------------------------+
| URL              | METHOD | ACTION                   |
+------------------+--------+--------------------------+
| /book            | POST   | bookController.create    |
| /book            | GET    | bookController.findAll   |
| /book/:id        | GET    | bookController.findOne   |
| /book/:id        | PUT    | bookController.update    |
| /book/:id        | DELETE | bookController.delete    |
+------------------+--------+--------------------------+

 */

// Book
basicRouter.post('/book', bookController.create);
basicRouter.get('/book', bookController.findAll);
basicRouter.get('/book/:id', bookController.findOne);
basicRouter.put('/book/:id', bookController.update);
basicRouter.delete('/book/:id', bookController.delete);

// Saga
basicRouter.post('/saga', sagaController.create);
basicRouter.get('/saga', sagaController.findAll);
basicRouter.get('/saga/:id', sagaController.findOne);
basicRouter.put('/saga/:id', sagaController.update);
basicRouter.delete('/saga/:id', sagaController.delete);

module.exports = basicRouter;
