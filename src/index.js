import express from 'express'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import mongoose from 'mongoose'

const app = express();
const port = process.env.port || 5000;
const basicRouter = require('../routes/basic.route'); // Peut se réduire ?

// MongoDB config
mongoose.connect('mongodb://localhost/biblio-ve');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('[MongoDB connected !!]');
});

app.use(bodyParser.json());
app.use(express.static(__dirname + '/../public'));
app.use(express.urlencoded({extended: false}));

// CUSTOM METHOD PUT, DELETE, ETC...
app.use(methodOverride((req, res) => {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}));

app.use('/', basicRouter);


app.listen(port, () => {
    console.log('[Application is running !!]');
});
