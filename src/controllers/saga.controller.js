const Saga = require('../models/saga');

module.exports = {

    create(req, res) {
        const newSaga = new Saga(req.body);

        newSaga.save((err, saga) => {
            if(err)
                res.send(err);
            res.json({
                message: 'The saga has been added successfully',
                saga
            });
        });
    },

    findOne(req, res) {
        Saga.findById(req.params.id, (err, saga) => {
            if(err)
                res.send(err);
            res.json(saga);
        });
    },

    findAll(req, res) {
        Saga.find({}, (err, sagas) => {
            if(err) 
                res.send(err);

            res.json(sagas);
        });
    },

    update(req, res) {
        Saga.findById({_id: req.params.id}, (err, saga) => {
            if(err)
                res.send(err);

            Object.assign(saga, req.body).save((err, saga) => {
                if(err)
                    res.send(err);
                res.json({message: 'The saga has been updated successfully', saga});
            });
        });
    },

    delete(req, res) {
        Saga.findOneAndDelete({_id: req.params.id}, (err, saga) => {
            if(err)
                res.send(err);

            res.json({saga: 'The saga has been deleted successfully', saga});
        });
    }

};
