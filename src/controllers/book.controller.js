const Book = require('../models/book');
const Saga = require('../models/saga');

module.exports = {

    create(req, res) {
        // Date parsing
        var date = new Date(req.body.publish_date);
        if( isNaN(date) )
            res.status(500).json('The publish date is not correctly formed');
        req.body.publish_date = date;

        const newBook = new Book(req.body);

        newBook.save((err, book) => {
            if(err)
                res.send(err);
            res.json({
                message: 'The book has been added successfully',
                book
            });
        });
    },

    findOne(req, res) {
        Book.findById(req.params.id, (err, book) => {
            if(err)
                res.send(err);

            Saga.findById(book.saga, (err, saga) => {
                if(err)
                    res.send('ERROR, Saga is not found');

                book.saga = saga;
            }).then(() => {
                res.json(book);
                console.log("YO");
            });

        });
    },

    findAll(req, res) {
        Book.find({}, (err, books) => {
            if(err) 
                res.send(err);

            res.json(books);
        });
    },

    update(req, res) {
        Book.findById({_id: req.params.id}, (err, book) => {
            if(err)
                res.send(err);

            // Date parsing
            var date = new Date(req.body.publish_date);
            if( isNaN(date) )
                res.status(500).json('The publish date is not correctly formed');
            req.body.publish_date = date;

            Object.assign(book, req.body).save((err, book) => {
                if(err)
                    res.send(err);
                res.json({message: 'The book has been updated successfully', book});
            });
        });
    },

    delete(req, res) {
        Book.findOneAndDelete({_id: req.params.id}, (err, book) => {
            if(err)
                res.send(err);

            res.json({message: 'The book has been deleted successfully', book});
        });
    }

};
