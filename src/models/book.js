import mongoose from 'mongoose'
const Schema = mongoose.Schema;

const BookSchema = new Schema({
    title: String,
    author: String,
    nb_page: Number,
    publish_date: Date,
    version: String,
    saga: { type: Schema.Types.ObjectId, ref: 'Saga' }
});

const Book = mongoose.model('Book', BookSchema);

/*
Book.getOneBook = async function(id) {

    Book.findById(id, (err, book) => {
        if(err)
            res.send('ERROR, Book is not found');

        book.saga = await getSaga(book.saga);

        return book;
    }

};

async function getSaga(id) {
    Saga.findById(id, (err, saga) => {
        if(err)
            throw new Error('Saga is not found');

        return saga;
    });
}
*/

module.exports = Book;
