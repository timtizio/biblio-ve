import mongoose from 'mongoose'
const Schema = mongoose.Schema;

const SagaSchema = new Schema({
    name: String,
});

const Saga = mongoose.model('Saga', SagaSchema);

module.exports = Saga;

